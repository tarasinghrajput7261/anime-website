document.querySelector('.menu-btn').addEventListener('click', () => {
  document.querySelector('.sidebar').classList.toggle('remove-sidebar');
  document.querySelector('.close-menu-btn').addEventListener('click', () => {
    document.querySelector('.sidebar').classList.add('remove-sidebar');
  })
})

// let screenWidth = screen.width;

// if (window.innerWidth <= "950") {
//   document.querySelector('.container').style.maxWidth = screenWidth;
// } else {

// }

// variable slideshowCont which stores slideshow content html
var slideshowCont = "";

const slideshowCaption = [
  {
    position: "#1 Spotlight", headingName: "Edens Zero 2nd Season", capDetail: "", capDescription: "Now in possession of the Edens Zero, Shiki Granbell has gathered the Four Shining Stars.With the help of his new friends, Shiki will be able to breach Dragonfall—the border of the Sakura Cosmos swarming with mechanical dragons. Once that is achieved, he can continue his quest to find the goddess Mother. Before the Edens Zero crew can advance their journey, they notice a mysterious warship following them. Upon learning that the ship...", 
    classNm: "first-slide", duration: "23m", date: "Mar 11, 2023", subbedEpNo: "8", dubbedEpNo: "0"
  },

  {
    position: "#2 Spotlight", headingName: "Bleach", capDetail: "", capDescription: "Ichigo Kurosaki is an ordinary high schooler—until his family is attacked by a Hollow, a corrupt spirit that seeks to devour human souls. It is then that he meets a Soul Reaper named Rukia Kuchiki, who gets injured while protecting Ichigo's family from the assailant. To save his family, Ichigo accepts Rukia's offer of taking her powers and becomes a Soul Reaper as a result....",
    classNm: "second-slide", duration: "24m", date: "Oct 5, 2004", subbedEpNo: "366", dubbedEpNo: "366"
  },

  {
    position: "#3 Spotlight", headingName: "Dr. Stone: New World", capDetail: "", capDescription: "With the ambitious Ryuusui Nanami on board, Senkuu Ishigami and his team are almost ready to sail the seas and reach the other side of the world—where the bizarre green light that petrified humanity originated. Thanks to the revival of a skillful chef, enough food is being prepared for the entire crew, and the incredible reinvention of the GPS promises to ensure safety on the open sea. Preparations for the upcoming journey progress swimmingly until...", classNm: "third-slide", duration: "23m", date: "Apr 6, 2023", subbedEpNo: "8", dubbedEpNo: "5"
  }
]

// for loop thats displays every slideshow multiple times
for (let i = 0; i < slideshowCaption.length; i++) {
 slideshowCont = slideshowCont +

    ` 
      <div class="mySlides fade ${slideshowCaption[i].classNm}">
        <div class="caption">
        <div class="position">${slideshowCaption[i].position}</div>
        <div class="heading-name"><h1>${slideshowCaption[i].headingName}</h1></div>
        <div class="cap-detail">
          <div class="anime-stream-type">
            <div class="tv"><img src="/images/png/play-button.png" alt="play" class="icon-xsm"> TV</div>
            <div class="duration"><img src="/images/png/clock.png" alt="clock" class="icon-xsm"> ${slideshowCaption[i].duration}</div>
            <div class="date"><img src="/images/png/calendar-wh.png" alt="calendar" class="icon-xsm"> ${slideshowCaption[i].date}</div>
            <div class="quality">HD</div>
            <div class="subbed-ep-no">
              <img src="/images/png/closed-caption-bl.png" alt="caption" class="icon-xsm"> ${slideshowCaption[i].subbedEpNo}
            </div>
            <div class="dubbed-ep-no">
              <img src="/images/png/mic-bl.png" alt="mic" class="icon-xsm"> ${slideshowCaption[i].dubbedEpNo}
            </div>
          </div>
          ${slideshowCaption[i].capDetail}</div>
        <div class="cap-description">
        ${slideshowCaption[i].capDescription}
        </div>
        <div class="cap-btns">
          <button class="cap-btn1"><img src="/images/svg/circle-play-solid.svg" alt="play" class="icon-sm"> Watch Now</button>
          <button class="cap-btn2">Detail  </button>
        </div>
        </div>
        <div class="controls prev" id="left-arrow" onclick="plusSlides(-1)">
          <img src="/images/png/left-arrow-wh.png" alt="left arrow" class="icon-xsm">
        </div>
        <div class="controls next" id="right-arrow" onclick="plusSlides(1)">
          <img src="/images/png/right-arrow-wh.png" alt="right arrow" class="icon-xsm">
        </div>
      </div>  
  `

}

// this statement calls the slideshow container div and fills the above for loop content as html in the container
document.getElementById('slideshow-cont').innerHTML = slideshowCont;


// starting slideshow JS
let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) { slideIndex = 1 }
  if (n < 1) { slideIndex = slides.length }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  // dots[slideIndex - 1].className += " active";
}
// ending slideshow JS




var trendingSectionSlideshow = "";

const trendingSlideshow = [
  {
    trendingPosition: "01", trendingAnimeName: "One Piece", img: "/images/png/trending-no-1.jpg"
  },
  {
    trendingPosition: "02", trendingAnimeName: "Demon...",
    img: "/images/png/trending-no-2.png"
  },
  {
    trendingPosition: "03", trendingAnimeName: "My Star", img: "/images/png/trending-no-3.jpg"
  },
  {
    trendingPosition: "04", trendingAnimeName: "I Got a...", 
    img: "/images/png/trending-no-4.jpg"
  },
  {
    trendingPosition: "05", trendingAnimeName: "Mashle...", img: "/images/png/trending-no-5.jpg"
  },
  {
    trendingPosition: "06", trendingAnimeName: "Hell's ...", img: "/images/png/trending-no-6.png"
  },
  {
    trendingPosition: "07", trendingAnimeName: "Dr. Stone...", img: "/images/png/trending-no-7.jpg"
  },
  {
    trendingPosition: "08", trendingAnimeName: "Blue Lock", img: "/images/png/trending-no-8.jpg"
  },
  {
    trendingPosition: "09", trendingAnimeName: "My Hero...", img: "/images/png/trending-no-9.jpg"
  },
  {
    trendingPosition: "10", trendingAnimeName: "Tokyo...", img: "/images/png/trending-no-10.jpg"
  }
];


// for loop thats displays every slideshow multiple times
for (let i = 0; i < trendingSlideshow.length; i++) {
  trendingSectionSlideshow = trendingSectionSlideshow + 
  `
    <div class="trending-img">
      <div class="trending-no-heading">${trendingSlideshow[i].trendingPosition}</div>
      <div class="trending-anime-name">${trendingSlideshow[i].trendingAnimeName}</div>
      <img src="${trendingSlideshow[i].img}" alt="trending-no-${trendingSlideshow[i].trendingPosition}">
    </div>
  `
}

// this statement calls the slideshow container div and fills the above for loop content as html in the container
document.getElementById('showcase-wrapper').innerHTML = trendingSectionSlideshow;


function prevTrendingScroll() {
  document.getElementById('trending-showcase-section').scrollLeft -= 270;
}

function nextTrendingScroll() {
  document.getElementById('trending-showcase-section').scrollLeft += 270;
}


// Top comments draggable slider
var topCommentsCarousel = "";

		const commentsCarousel = [
			{
				commentAvatar: '/images/avatar/comments-avatar-01.jpg', commentProfileName: 'TARA', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-02.png', commentProfileName: 'Aniket_Luffy', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-03.png', commentProfileName: 'I.AM.ATONIC', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-04.jpeg', commentProfileName: 'ananth_by_anime', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-05.png', commentProfileName: 'Nick', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-06.png', commentProfileName: 'LilMonster', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-07.jpeg', commentProfileName: 'Respawn', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-08.jpg', commentProfileName: 'Sagar_Uchiha', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-09.jpeg', commentProfileName: 'Pranav_Namikaze', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-10.png', commentProfileName: '👑KING👑', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-11.jpg', commentProfileName: 'Luffy', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-12.png', commentProfileName: 'Gloxy', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-13.png', commentProfileName: 'OtakuAbhinav', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-14.jpg', commentProfileName: 'Jerry', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-15.jpg', commentProfileName: 'Harsh_Uzumaki', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-16.png', commentProfileName: 'Vidhyanshu_King', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-17.jpg', commentProfileName: 'Yash_The_Hokage', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
			{
				commentAvatar: '/images/avatar/comments-avatar-18.jpg', commentProfileName: 'Rishu_The_Last_Uchiha', comment: 'Top Pervert is Jiraya',
				topCommentAnimeName: 'Naruto Shippuden'
			},
		];

		// for loop thats displays every slideshow multiple times
		for (let i = 0; i < commentsCarousel.length; i++) {
			topCommentsCarousel = topCommentsCarousel +
				`
				<div class="card">
					<img src="${commentsCarousel[i].commentAvatar}" alt="" class="comments-avatar">
						<div class="comments-name-section">
							<h5>${commentsCarousel[i].commentProfileName}</h5>
							<p> - 2 minutes ago</p>
						</div>
						<div class="top-comments">${commentsCarousel[i].comment}</div>
						<div class="comment-anime-name">
							<img src="/images/png/google-docs.png" alt="" class="icon-xsm">
								<p>${commentsCarousel[i].topCommentAnimeName}</p>
						</div>
		
				</div>
				`
		}

		// this statement calls the slideshow container div and fills the above for loop content as html in the container
		document.getElementById('comments-carousel').innerHTML = topCommentsCarousel;



		const carousel = document.querySelector(".comments-carousel-section"),
			// the line below returns 1st img we can use 'document.querySelector('img')'
			firstImg = document.querySelector('.card')[0];
		// arrowIcons = document.querySelectorAll(".wrapper i");
		let isDragStart = false, isDragging = false, prevPageX, prevScrollLeft, positionDiff;
		// const showHideIcons = () => {
		// 	// showing and hiding prev/next icon according to carousel scroll left value
		// 	let scrollWidth = carousel.scrollWidth - carousel.clientWidth; // getting max scrollable width
		// 	arrowIcons[0].style.display = carousel.scrollLeft == 0 ? "none" : "block";
		// 	arrowIcons[1].style.display = carousel.scrollLeft == scrollWidth ? "none" : "block";
		// }

		// arrowIcons.forEach(icon => {
		// 	icon.addEventListener("click", () => {
		// 		let firstImgWidth = firstImg.clientWidth + 14; // getting first img width & adding 14 margin value
		// 		// if clicked icon is left, reduce width value from the carousel scroll left else add to it
		// 		carousel.scrollLeft += icon.id == "left" ? -firstImgWidth : firstImgWidth;
		// 		setTimeout(() => showHideIcons(), 60); // calling showHideIcons after 60ms
		// 	});
		// });

		// const autoSlide = () => {
		// 	// if there is no image left to scroll then return from here
		// 	if (carousel.scrollLeft - (carousel.scrollWidth - carousel.clientWidth) > -1 || carousel.scrollLeft <= 0) return;

		// 	positionDiff = Math.abs(positionDiff); // making positionDiff value to positive
		// 	let firstImgWidth = firstImg.clientWidth + 14;
		// 	// getting difference value that needs to add or reduce from carousel left to take middle img center
		// 	let valDifference = firstImgWidth - positionDiff;

		// 	if (carousel.scrollLeft > prevScrollLeft) { // if user is scrolling to the right
		// 		return carousel.scrollLeft += positionDiff > firstImgWidth / 3 ? valDifference : -positionDiff;
		// 	}
		// 	// if user is scrolling to the left
		// 	carousel.scrollLeft -= positionDiff > firstImgWidth / 3 ? valDifference : -positionDiff;
		// }

		const dragStart = (e) => {
			// updatating global variables value on mouse down event
			isDragStart = true;
			prevPageX = e.pageX || e.touches[0].pageX;
			prevScrollLeft = carousel.scrollLeft;
		}

		const dragging = (e) => {
			// scrolling images/carousel to left according to mouse pointer
			if (!isDragStart) return;
			e.preventDefault();
			isDragging = true;
			carousel.classList.add("dragging");
			positionDiff = (e.pageX || e.touches[0].pageX) - prevPageX;
			carousel.scrollLeft = prevScrollLeft - positionDiff;
			// showHideIcons();
		}

		const dragStop = () => {
			isDragStart = false;
			carousel.classList.remove("dragging");

			if (!isDragging) return;
			isDragging = false;
			// autoSlide();
		}

		carousel.addEventListener("mousedown", dragStart);
		carousel.addEventListener("touchstart", dragStart);

		document.addEventListener("mousemove", dragging);
		carousel.addEventListener("touchmove", dragging);

		document.addEventListener("mouseup", dragStop);
		carousel.addEventListener("touchend", dragStop);



